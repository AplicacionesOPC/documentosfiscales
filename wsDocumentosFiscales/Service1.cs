﻿using System;
using System.Data;
using System.IO;
using System.ServiceProcess;

namespace wsDocumentosFiscales
{
    public partial class Service1 : ServiceBase
    {
        fSqlConnect conx = new fSqlConnect("usrwebservice", "W3bs3rv1c3");

        private System.Threading.Thread hiloFacturas;
        private System.Threading.Thread hiloNotasDeCredito;

        private System.Threading.Thread hiloCambioDolar_PROD;

        private System.Timers.Timer timerFacturas = new System.Timers.Timer();
        private System.Timers.Timer timerNotasDeCredito = new System.Timers.Timer();

        private System.Timers.Timer timerCambioDolar_PROD = new System.Timers.Timer();

        public Service1()
        {
            InitializeComponent();
        }

        private void PrepareTask_ActualizaFacturas()
        {
            timerFacturas.Elapsed += new System.Timers.ElapsedEventHandler(timerFacturas_Elapsed);
            timerFacturas.Interval = 60000;
            timerFacturas.Start();
            System.Threading.Thread.Sleep(System.Threading.Timeout.Infinite);
        }

        private void PrepareTask_NotasDeCredito()
        {
            timerNotasDeCredito.Elapsed += new System.Timers.ElapsedEventHandler(timerNotasDeCredito_Elapsed);
            timerNotasDeCredito.Interval = 65000;
            timerNotasDeCredito.Start();
            System.Threading.Thread.Sleep(System.Threading.Timeout.Infinite);
        }

        private void PrepareTask_CambioDolar_PROD()
        {
            timerCambioDolar_PROD.Elapsed += new System.Timers.ElapsedEventHandler(timerCambioDolar_Elapsed);
            timerCambioDolar_PROD.Interval = 55000;
            timerCambioDolar_PROD.Start();
            System.Threading.Thread.Sleep(System.Threading.Timeout.Infinite);
        }

        protected override void OnStart(string[] args)
        {
            hiloFacturas = new System.Threading.Thread(PrepareTask_ActualizaFacturas);
            hiloFacturas.Start();

            hiloNotasDeCredito = new System.Threading.Thread(PrepareTask_NotasDeCredito);
            hiloNotasDeCredito.Start();

            //hiloCambioDolar_PROD = new System.Threading.Thread(PrepareTask_CambioDolar_PROD);
            //hiloCambioDolar_PROD.Start();

            LogsService("start service");
        }

        public void OnDebug()
        {
            OnStart(null);
        }

        protected override void OnStop()
        {
            LogsService("stopping service");
        }

        private void LogsService(string content)
        {
            FileStream fs = new FileStream(@"E:\Logs\Documentos Fiscales\ScheduledService.txt", FileMode.OpenOrCreate, FileAccess.Write);
            StreamWriter sw = new StreamWriter(fs);

            sw.BaseStream.Seek(0, SeekOrigin.End);
            sw.WriteLine(content);
            sw.Flush();

            sw.Close();
        }
        private void LogsServiceFacturas(string content)
        {
            FileStream fs = new FileStream(@"E:\Logs\Documentos Fiscales\FacturasService.txt", FileMode.OpenOrCreate, FileAccess.Write);
            StreamWriter sw = new StreamWriter(fs);

            sw.BaseStream.Seek(0, SeekOrigin.End);
            sw.WriteLine(content);
            sw.Flush();

            sw.Close();
        }
        private void LogsServiceCambioDolar(string content)
        {
            FileStream fs = new FileStream(@"E:\Logs\CambioDolar\ScheduledService.txt", FileMode.OpenOrCreate, FileAccess.Write);
            StreamWriter sw = new StreamWriter(fs);

            sw.BaseStream.Seek(0, SeekOrigin.End);
            sw.WriteLine(content);
            sw.Flush();

            sw.Close();
        }

        private void EjecutaCambioDolar()
        {
            string sql = "";

            DataSet ds = new DataSet();

            srDocumentosFiscales.DocumentosClient xDocFiscales = new srDocumentosFiscales.DocumentosClient();

            sql = "Exec dbo.sp_Aplicar_CambioDolar_Factura";

            try
            {
                ds = conx.getDatasetPROD(sql);
                LogsServiceCambioDolar("" + DateTime.Now + " - " + "Ejecutando Procedimiento CambioDolar | Cant: " + ds.Tables[0].Rows.Count.ToString());
            }
            catch (Exception ex)
            {
                LogsServiceCambioDolar("" + DateTime.Now + " - " + "Error en CambioDolar: " + ex.Message);
            }
        }

        private void ActualizaNotasDeCredito()
        {
            string sql = "";
            string xgkey = "";
            string xDocumento = "";
            string xCorrelativo = "";

            DataSet ds = new DataSet();
            DataSet dsEject = new DataSet();

            srDocumentosFiscales.DocumentosClient xDocFiscales = new srDocumentosFiscales.DocumentosClient();

            sql = "Exec dbo.sp_Lista_Notas_de_Credito_DEI";

            try
            {
                ds = conx.getDatasetPROD(sql);
                LogsService("" + DateTime.Now + " - " + "Ejecutando Procedimiento NotasDeCredito | Cant: " + ds.Tables[0].Rows.Count.ToString());

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    xgkey = dr["credit_gkey"].ToString();
                    xDocumento = dr["final_nbr"].ToString();
                    xCorrelativo = xDocFiscales.getCorrelativoPROD("002", "06", xDocumento);

                    if (xCorrelativo != "")
                    {
                        sql = String.Format("Exec dbo.sp_Actualiza_Notas_de_Credito_DEI {0},'{1}','{2}'", xgkey, xDocumento, xCorrelativo);
                        dsEject = conx.getDatasetPROD(sql);

                        LogsService("" + DateTime.Now + " - " + "Insert data NotasDeCredito: " + xDocumento + " | " + xCorrelativo);
                    }
                    else
                    {
                        LogsService("" + DateTime.Now + " - " + "No hay correlativo disponible. NotaDeCredito: " + xDocumento);
                    }
                }
            }
            catch (Exception ex)
            {
                LogsService("" + DateTime.Now + " - " + "Error en ActualizaNotasDeCredito: " + ex.Message);
            }
        }
        private void ActualizaFacturas()
        {
            timerFacturas.Stop();
            string sql = "";
            string xgkey = "";
            string xDocumento = "";
            string xCorrelativo = "";
            string xLeyenda = "";

            DataSet ds = new DataSet();
            DataSet dsEject = new DataSet();
            DataSet dsLeyenda = new DataSet();

            srDocumentosFiscales.DocumentosClient xDocFiscales = new srDocumentosFiscales.DocumentosClient();

            sql = "Exec dbo.sp_Lista_Facturas_DEI";

            try
            {

                ds = conx.getDatasetPROD(sql);
                LogsServiceFacturas("" + DateTime.Now + " - " + "Ejecutando Procedimiento Facturas | Cant: " + ds.Tables[0].Rows.Count.ToString());

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    xgkey = dr["invoice_gkey"].ToString();
                    xDocumento = dr["final_nbr"].ToString();
                    xCorrelativo = xDocFiscales.getCorrelativoPROD("002", "01", xDocumento);
                    LogsServiceFacturas("" + DateTime.Now + " - " + "DatoServicio: " + xCorrelativo);

                    if (xCorrelativo != "")
                    {
                        sql = String.Format("Exec dbo.sp_lista_Leyenda_Facturas_DEI {0},'{1}','{2}'", xgkey, xDocumento, xCorrelativo);
                        dsLeyenda = conx.getDatasetPROD(sql);
                        LogsServiceFacturas("" + DateTime.Now + " - " + "Ejecutando Procedimiento Leyenda | Cant: " + dsLeyenda.Tables[0].Rows.Count.ToString());

                        foreach (DataRow drLeyenda in dsLeyenda.Tables[0].Rows)
                        {
                            xLeyenda = drLeyenda["TEXTO1"].ToString();

                            sql = String.Format("Exec dbo.sp_Actualiza_Facturas_DEI {0},'{1}','{2}','{3}'", xgkey, xDocumento, xCorrelativo, xLeyenda);
                            dsEject = conx.getDatasetBilling(sql);

                            LogsServiceFacturas("" + DateTime.Now + " - " + "Insert data Facturas: " + xDocumento + " | " + xCorrelativo);
                        }
                    }
                    else
                    {
                        LogsServiceFacturas("" + DateTime.Now + " - " + "No hay correlativo disponible. Factura: " + xDocumento);
                    }
                }
            }
            catch (Exception ex)
            {
                LogsServiceFacturas("" + DateTime.Now + " - " + "Error en ActualizaFacturas: " + ex.Message);
            }
            timerFacturas.Start();
        }

        private void timerCambioDolar_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                EjecutaCambioDolar();
            }
            catch (Exception ex)
            {
                LogsService("" + ex.Message + " " + DateTime.Now);
            }
        }

        private void timerFacturas_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                ActualizaFacturas();
            }
            catch (Exception ex)
            {
                LogsService("" + ex.Message + " " + DateTime.Now);
            }
        }

        private void timerNotasDeCredito_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                ActualizaNotasDeCredito();
            }
            catch (Exception ex)
            {
                LogsService("" + ex.Message + " " + DateTime.Now);
            }
        }
    }
}
