﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace wsDocumentosFiscales
{
    class fSqlConnect
    {
        public bool acceso = false;

        private string usuario;
        public string Usuario
        {
            get { return usuario; }
            set { usuario = value; }
        }

        private string password;
        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        private string connectionStringDESA = "Data Source=dataserverapps.opc.local;Initial Catalog=WebService_DESA;User Id=usrInterface;Password=1nt3rf4c3;";
        private string connectionStringPROD = "Data Source=dataserverapps.opc.local;Initial Catalog=WebService_PROD;User Id=usrInterface;Password=1nt3rf4c3;";
        private string connectionStringBilling = "Data Source=HNOPCSQAG01.opc.local;Initial Catalog=Billing;User Id=usrInterface;Password=1nt3rf4c3;";

        public DataSet getDatasetDESA(string sp)
        {
            SqlConnection con = new SqlConnection(connectionStringDESA);
            SqlCommand cmd = new SqlCommand(sp, con);
            DataSet ds;
            try
            {
                con.Open();

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 120;
                SqlDataReader dr = cmd.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Load(dr);
                ds = new DataSet();
                ds.Tables.Add(dt);
                return ds;
            }
            finally
            {
                con.Dispose();
                cmd.Dispose();
            }
        }

        public DataSet getDatasetPROD(string sp)
        {
            SqlConnection con = new SqlConnection(connectionStringPROD);
            SqlCommand cmd = new SqlCommand(sp, con);
            DataSet ds;
            try
            {
                con.Open();

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 120;
                SqlDataReader dr = cmd.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Load(dr);
                ds = new DataSet();
                ds.Tables.Add(dt);
                return ds;
            }
            finally
            {
                con.Dispose();
                cmd.Dispose();
            }
        }

        public DataSet getDatasetBilling(string sp)
        {
            SqlConnection con = new SqlConnection(connectionStringBilling);
            SqlCommand cmd = new SqlCommand(sp, con);
            DataSet ds;
            try
            {
                con.Open();

                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 120;
                SqlDataReader dr = cmd.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Load(dr);
                ds = new DataSet();
                ds.Tables.Add(dt);
                return ds;
            }
            finally
            {
                con.Dispose();
                cmd.Dispose();
            }
        }

        public fSqlConnect(string user, string pass)
        {
            Usuario = user;
            Password = pass;

            acceso = false;
        }
    }
}
