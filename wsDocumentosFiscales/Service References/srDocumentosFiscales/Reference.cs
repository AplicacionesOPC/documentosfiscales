﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace wsDocumentosFiscales.srDocumentosFiscales {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="srDocumentosFiscales.IDocumentos")]
    public interface IDocumentos {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IDocumentos/DoWork", ReplyAction="http://tempuri.org/IDocumentos/DoWorkResponse")]
        void DoWork();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IDocumentos/DoWork", ReplyAction="http://tempuri.org/IDocumentos/DoWorkResponse")]
        System.Threading.Tasks.Task DoWorkAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IDocumentos/getCorrelativo", ReplyAction="http://tempuri.org/IDocumentos/getCorrelativoResponse")]
        string getCorrelativo(string codEstablecimiento, string codDocumento, string nroDocumento);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IDocumentos/getCorrelativo", ReplyAction="http://tempuri.org/IDocumentos/getCorrelativoResponse")]
        System.Threading.Tasks.Task<string> getCorrelativoAsync(string codEstablecimiento, string codDocumento, string nroDocumento);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IDocumentos/getCorrelativoPROD", ReplyAction="http://tempuri.org/IDocumentos/getCorrelativoPRODResponse")]
        string getCorrelativoPROD(string codEstablecimiento, string codDocumento, string nroDocumento);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IDocumentos/getCorrelativoPROD", ReplyAction="http://tempuri.org/IDocumentos/getCorrelativoPRODResponse")]
        System.Threading.Tasks.Task<string> getCorrelativoPRODAsync(string codEstablecimiento, string codDocumento, string nroDocumento);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IDocumentos/getCorrelativoAuth", ReplyAction="http://tempuri.org/IDocumentos/getCorrelativoAuthResponse")]
        string getCorrelativoAuth(string user, string password, string codEstablecimiento, string codDocumento, string nroDocumento);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IDocumentos/getCorrelativoAuth", ReplyAction="http://tempuri.org/IDocumentos/getCorrelativoAuthResponse")]
        System.Threading.Tasks.Task<string> getCorrelativoAuthAsync(string user, string password, string codEstablecimiento, string codDocumento, string nroDocumento);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IDocumentos/getCorrelativoPRODAuth", ReplyAction="http://tempuri.org/IDocumentos/getCorrelativoPRODAuthResponse")]
        string getCorrelativoPRODAuth(string user, string password, string codEstablecimiento, string codDocumento, string nroDocumento);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IDocumentos/getCorrelativoPRODAuth", ReplyAction="http://tempuri.org/IDocumentos/getCorrelativoPRODAuthResponse")]
        System.Threading.Tasks.Task<string> getCorrelativoPRODAuthAsync(string user, string password, string codEstablecimiento, string codDocumento, string nroDocumento);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IDocumentos/GetCai", ReplyAction="http://tempuri.org/IDocumentos/GetCaiResponse")]
        string GetCai(string codEstablecimiento, string codDocumento, string correlativoDei);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IDocumentos/GetCai", ReplyAction="http://tempuri.org/IDocumentos/GetCaiResponse")]
        System.Threading.Tasks.Task<string> GetCaiAsync(string codEstablecimiento, string codDocumento, string correlativoDei);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IDocumentos/GetCaiPROD", ReplyAction="http://tempuri.org/IDocumentos/GetCaiPRODResponse")]
        string GetCaiPROD(string codEstablecimiento, string codDocumento, string correlativoDei);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IDocumentos/GetCaiPROD", ReplyAction="http://tempuri.org/IDocumentos/GetCaiPRODResponse")]
        System.Threading.Tasks.Task<string> GetCaiPRODAsync(string codEstablecimiento, string codDocumento, string correlativoDei);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IDocumentos/GetDollarTrendLine", ReplyAction="http://tempuri.org/IDocumentos/GetDollarTrendLineResponse")]
        decimal GetDollarTrendLine(System.DateTime FechaProyectada);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IDocumentos/GetDollarTrendLine", ReplyAction="http://tempuri.org/IDocumentos/GetDollarTrendLineResponse")]
        System.Threading.Tasks.Task<decimal> GetDollarTrendLineAsync(System.DateTime FechaProyectada);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IDocumentosChannel : wsDocumentosFiscales.srDocumentosFiscales.IDocumentos, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class DocumentosClient : System.ServiceModel.ClientBase<wsDocumentosFiscales.srDocumentosFiscales.IDocumentos>, wsDocumentosFiscales.srDocumentosFiscales.IDocumentos {
        
        public DocumentosClient() {
        }
        
        public DocumentosClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public DocumentosClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public DocumentosClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public DocumentosClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public void DoWork() {
            base.Channel.DoWork();
        }
        
        public System.Threading.Tasks.Task DoWorkAsync() {
            return base.Channel.DoWorkAsync();
        }
        
        public string getCorrelativo(string codEstablecimiento, string codDocumento, string nroDocumento) {
            return base.Channel.getCorrelativo(codEstablecimiento, codDocumento, nroDocumento);
        }
        
        public System.Threading.Tasks.Task<string> getCorrelativoAsync(string codEstablecimiento, string codDocumento, string nroDocumento) {
            return base.Channel.getCorrelativoAsync(codEstablecimiento, codDocumento, nroDocumento);
        }
        
        public string getCorrelativoPROD(string codEstablecimiento, string codDocumento, string nroDocumento) {
            return base.Channel.getCorrelativoPROD(codEstablecimiento, codDocumento, nroDocumento);
        }
        
        public System.Threading.Tasks.Task<string> getCorrelativoPRODAsync(string codEstablecimiento, string codDocumento, string nroDocumento) {
            return base.Channel.getCorrelativoPRODAsync(codEstablecimiento, codDocumento, nroDocumento);
        }
        
        public string getCorrelativoAuth(string user, string password, string codEstablecimiento, string codDocumento, string nroDocumento) {
            return base.Channel.getCorrelativoAuth(user, password, codEstablecimiento, codDocumento, nroDocumento);
        }
        
        public System.Threading.Tasks.Task<string> getCorrelativoAuthAsync(string user, string password, string codEstablecimiento, string codDocumento, string nroDocumento) {
            return base.Channel.getCorrelativoAuthAsync(user, password, codEstablecimiento, codDocumento, nroDocumento);
        }
        
        public string getCorrelativoPRODAuth(string user, string password, string codEstablecimiento, string codDocumento, string nroDocumento) {
            return base.Channel.getCorrelativoPRODAuth(user, password, codEstablecimiento, codDocumento, nroDocumento);
        }
        
        public System.Threading.Tasks.Task<string> getCorrelativoPRODAuthAsync(string user, string password, string codEstablecimiento, string codDocumento, string nroDocumento) {
            return base.Channel.getCorrelativoPRODAuthAsync(user, password, codEstablecimiento, codDocumento, nroDocumento);
        }
        
        public string GetCai(string codEstablecimiento, string codDocumento, string correlativoDei) {
            return base.Channel.GetCai(codEstablecimiento, codDocumento, correlativoDei);
        }
        
        public System.Threading.Tasks.Task<string> GetCaiAsync(string codEstablecimiento, string codDocumento, string correlativoDei) {
            return base.Channel.GetCaiAsync(codEstablecimiento, codDocumento, correlativoDei);
        }
        
        public string GetCaiPROD(string codEstablecimiento, string codDocumento, string correlativoDei) {
            return base.Channel.GetCaiPROD(codEstablecimiento, codDocumento, correlativoDei);
        }
        
        public System.Threading.Tasks.Task<string> GetCaiPRODAsync(string codEstablecimiento, string codDocumento, string correlativoDei) {
            return base.Channel.GetCaiPRODAsync(codEstablecimiento, codDocumento, correlativoDei);
        }
        
        public decimal GetDollarTrendLine(System.DateTime FechaProyectada) {
            return base.Channel.GetDollarTrendLine(FechaProyectada);
        }
        
        public System.Threading.Tasks.Task<decimal> GetDollarTrendLineAsync(System.DateTime FechaProyectada) {
            return base.Channel.GetDollarTrendLineAsync(FechaProyectada);
        }
    }
}
